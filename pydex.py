#!/usr/bin/env python3
# Copyright (C) 2020 Alexis Lockwood, <alexlockwood@fastmail.com>
# Based on MemcardRex/Hardware/DexDrive.cs, also GPL3
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.
#
# Why
# ---
#
# Managing PSX memory cards on Linux is a pain. MemcardRex and PSXGameEdit under
# Wine can manipulate save data, but have trouble working the serial port to
# operate a DexDrive to access the card.
#
# This tool does not (yet?) provide file management within the card. It can copy
# specified slots, but can't move them around - they must go into the same slot
# numbers they start from.
#
# Note on linked blocks
# ---------------------
#
# The PSX memory card format uses linked lists to group slots into larger
# files. Because this tool doesn't provide file management you need a basic
# understanding of this to copy larger files.
#
# When reading out a card using the `list` command, the first slot in a
# file can be identified by name. Subsequent blocks in the file are identified
# by following the LINK pointers from one block number to the next, up to the
# last block with a null pointer. All of these blocks must be copied in place.

import argparse
import enum
import logging
import os
import platform
import struct
import sys
import time

import serial

QUIET = False

FRAME_SIZE = 128
BLOCK_SIZE = 8192
TOTAL_SIZE = 131072

def status(*args, **kwargs):
    if not QUIET:
        print(*args, **kwargs)

def frame_addr(frame):
    """Return the address slice for a frame"""
    return slice(
        FRAME_SIZE * frame,
        FRAME_SIZE * (frame + 1),
    )

def dirframe_addr(slot):
    """Return the address slice for a directory frame"""
    return frame_addr(slot + 1)

def slot_addr(slot):
    """Return the address slice for a slot's data block"""
    return slice(
        BLOCK_SIZE * (slot + 1),
        BLOCK_SIZE * (slot + 2),
    )

def xor_frame(mc, frame):
    """Update the xor byte in a frame.

    mc: Memory card
    frame: Frame number
    """

    addrs = frame_addr(frame)

    xor = 0
    for i in mc[addrs.start : addrs.stop - 1]:
        xor ^= i

    mc[addrs.stop - 1] = xor

class DexError(Exception):
    pass

class DexHardwareError(DexError):
    pass

class DexCommError(DexError):
    pass

class DexMediaError(DexError):
    pass

class DexCommand(enum.Enum):
    INIT            = 0x00
    STATUS          = 0x01
    READ            = 0x02
    WRITE           = 0x04
    LIGHT           = 0x07
    MAGIC_HANDSHAKE = 0x27

class DexResponse(enum.Enum):
    POUT        = 0x20
    ERROR       = 0x21
    NOCARD      = 0x22
    CARD        = 0x23
    WRITE_OK    = 0x28
    WRITE_SAME  = 0x29
    WAIT        = 0x2A
    ID          = 0x40
    DATA        = 0x41

class DexDrive:
    def __init__(self, path):
        """Initialize the DexDrive.

        path: Path to the serial port
        """

        self.port = serial.Serial(
            path,
            baudrate = 38400,
            timeout = 0,
        )

        # Dexdrive won't respond if RTS is not toggled on/off
        self.port.rts = False
        time.sleep(0.3)
        self.port.rts = True
        time.sleep(0.3)

        # DTR line is used for additional power
        self.port.dtr = True

        # Check if DexDrive is attached to the port
        # Detection may fail 1st or 2nd time, so the command is sent
        # 5 times
        for i in range(5):
            self.port.reset_input_buffer()
            self.port.write(b"XXXXX")
            time.sleep(0.02)

        # Check for "IAI" string
        if self.read_data_from_port()[0:3] != b"IAI":
            raise DexHardwareError(f"DexDrive was not detected on {path}")

        # Wake DexDrive up (kick it from POUT mode)
        self.send_data_to_port(
            DexCommand.INIT,
            bytes([
                0x10, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0xAA, 0xBB, 0xCC,
                0xDD, 0xEE, 0xFF, 0xAA, 0xBB, 0xCC, 0xDD,
            ]),
            0.05,
        )

        # Check for "PSX" string
        data = self.read_data_from_port()
        if data[5:8] != b"PSX":
            raise DexHardwareError("Detected device is not a PS1 DexDrive.")

        # Fetch the firmware version
        self.firmware_version = (
            f"{data[8] >> 6}.{(data[8] >> 2) & 0xF}{data[8] & 0x3}"
        )
        print(f"fw ver: {self.firmware_version}")

        # Send magic handshake signal 10 times
        for i in range(10):
            self.send_data_to_port(
                DexCommand.MAGIC_HANDSHAKE,
                None,
                0,
            )
        time.sleep(0.05)

        self.light(True)
        self.open = True

    def __del__(self):
        self.close()

    def close(self):
        if self.open:
            self.light(False)
            self.port.close()
            self.open = False

    def light(self, on):
        """Turn the light on or off"""
        self.send_data_to_port(
            DexCommand.LIGHT,
            b"\x01" if on else b"\x00",
            0.05,
        )

    def send_data_to_port(self, command, data, delay):
        """Send DexDrive command on the opened COM port with a delay"""

        packet = b"IAI" + bytes([command.value])

        if data:
            packet += data

        logging.debug(f"Sending {packet!r} and waiting {delay} s")

        self.port.reset_input_buffer()
        self.port.write(packet)

        if delay:
            time.sleep(delay)

    def read_data_from_port(self):
        """Catch the response from a DexDrive"""

        data = self.port.read(256)
        logging.debug(f"Received: {data!r}")

        return data

    def read_memory_card_frame(self, num):
        frame_lsb = num & 0xFF
        frame_msb = (num >> 8) & 0xFF
        xor_data = frame_lsb ^ frame_msb

        self.send_data_to_port(
            DexCommand.READ,
            bytes([frame_lsb, frame_msb]),
            0,
        )

        old_timeout = self.port.timeout
        self.port.timeout = 0.1
        data = self.read_data_from_port()
        self.port.timeout = old_timeout

        if data[0:3] != b"IAI":
            raise DexCommError("Corrupted response, expected IAI")

        if data[3] == DexResponse.NOCARD.value:
            raise DexMediaError("No card present")

        card_data = data[4:132]
        for i in card_data:
            xor_data ^= i

        if xor_data != data[132]:
            raise DexCommError(f"Corrupted response, checksum mismatch: {xor_data} != {data[132]}")

        return card_data

    def write_memory_card_frame(self, num, data):
        if not isinstance(data, (bytes, bytearray)):
            raise TypeError("data must be bytes")

        if len(data) != 128:
            raise ValueError("data must be exactly 128 bytes long")

        frame_lsb = num & 0xFF
        frame_msb = (num >> 8) & 0xFF
        rev_frame_lsb = int(f"{frame_lsb:08b}"[::-1], 2)
        rev_frame_msb = int(f"{frame_msb:08b}"[::-1], 2)
        xor_data = frame_msb ^ frame_lsb ^ rev_frame_msb ^ rev_frame_lsb

        for i in data:
            xor_data ^= i

        packet = (
            bytes([frame_msb, frame_lsb, rev_frame_msb, rev_frame_lsb])
            + data
            + bytes([xor_data])
        )

        self.send_data_to_port(
            DexCommand.WRITE,
            packet,
            0,
        )

        old_timeout = self.port.timeout
        self.port.timeout = 0.1
        read_data = self.read_data_from_port()
        self.port.timeout = old_timeout

        # Check the return status (return true if all went OK)
        if (read_data[3] == DexResponse.WRITE_OK.value or
            read_data[3] == DexResponse.WRITE_SAME.value):
            return True

        if read_data[3] == DexResponse.NOCARD.value:
            raise DexMediaError("No card present")

        return False

class BlockRWBuffer:
    """Buffered read-write adapter that reads and writes in blocks, caching
    data to avoid unnecessary transactions.
    """

    def __init__(self):
        self.data = bytearray(self.block_size() * self.len())
        self.is_read = bytearray(self.block_size() * self.len())
        self.needs_flush = bytearray(self.block_size() * self.len())

    def block_size(self):
        raise NotImplementedError

    def read_block(self, n):
        raise NotImplementedError

    def len(self):
        """Length in blocks"""
        raise NotImplementedError

    def write_block(self, n, v):
        raise NotImplementedError

    def sync_read(self, start, stop, /, writing):
        """Ensure all bytes from start to stop are present

        writing: if True, this is a presync for a write. In that case, blocks
            will only be synced if the write covers them PARTIALLY. This avoids
            reading in data that is to be immediately discarded
        """

        bs = self.block_size()
        start_block = start // bs
        stop_block = (stop + bs - 1) // bs

        for block in range(start_block, stop_block):
            start_byte = block * bs
            stop_byte = (block + 1) * bs

            if writing and (start_byte >= start and stop_byte <= stop):
                self.is_read[start_byte:stop_byte] = b"\x01" * bs
                continue

            if not all(self.is_read[start_byte:stop_byte]):
                self.data[start_byte:stop_byte] = self.read_block(block)
                self.is_read[start_byte:stop_byte] = b"\x01" * bs

    def __getitem__(self, n):
        if not isinstance(n, slice):
            n = slice(n, n + 1)

        if n.step is not None and s.step != 1:
            raise ValueError("Stepped slices not implemented")

        self.sync_read(n.start, n.stop, writing=False)

        return self.data[n]

    def __setitem__(self, n, v):
        if not isinstance(n, slice):
            n = slice(n, n + 1)
            v = [v]

        if n.step is not None and s.step != 1:
            raise ValueError("Stepped slices not implemented")

        self.sync_read(n.start, n.stop, writing=True)

        self.data[n] = v
        self.needs_flush[n] = b"\x01" * (n.stop - n.start)

    def flush(self):
        bs = self.block_size()

        for block in range(self.len()):
            start_byte = block * bs
            stop_byte = (block + 1) * bs
            if any(self.needs_flush[start_byte:stop_byte]):
                self.write_block(block, self.data[start_byte:stop_byte])
                self.needs_flush[start_byte:stop_byte] = b"\x00" * bs

class MemcardFileRWBuffer(BlockRWBuffer):
    def __init__(self, fn, write):
        super().__init__()
        if not write:
            self.f = open(fn, "rb")
        else:
            try:
                self.f = open(fn, "r+b")
            except FileNotFoundError:
                self.f = open(fn, "w+b")
                self.f.truncate(TOTAL_SIZE)

    def block_size(self):
        return FRAME_SIZE

    def read_block(self, n):
        logging.info(f"Reading block {n} from {self}...")
        self.f.seek(n * FRAME_SIZE)
        return self.f.read(FRAME_SIZE)

    def len(self):
        """Length in blocks"""
        return TOTAL_SIZE // FRAME_SIZE

    def write_block(self, n, v):
        logging.info(f"Writing block {n} to {self}...")
        self.f.seek(n * FRAME_SIZE)
        self.f.write(v)
        self.f.flush()

class DexdriveRWBuffer(BlockRWBuffer):
    def __init__(self, dd):
        super().__init__()
        self.dd = dd

    def block_size(self):
        return FRAME_SIZE

    def read_block(self, n):
        logging.info(f"Reading block {n} from {self}...")
        return self.dd.read_memory_card_frame(n)

    def len(self):
        """Length in blocks"""
        return TOTAL_SIZE // FRAME_SIZE

    def write_block(self, n, v):
        logging.info(f"Writing block {n} to {self}...")
        return self.dd.write_memory_card_frame(n, v)

class MemcardDir:
    def __init__(self, mc, i):
        """Initialize, given a memory card (BlockRWBuffer object) and index."""
    
        dirframe = mc[dirframe_addr(i)]
        (
            raw_usage,
            raw_reserved,
            raw_chainlen,
            raw_link,
            raw_country,
            raw_product,
            raw_ident,
            raw_unused,
            raw_xor
        ) = struct.unpack("<B3sIH2s10s8s97sB", dirframe)

        self.n = i
        self.reserved = False

        if (raw_usage & 0xF0) == 0xA0:
            self.availability = "free"
        elif (raw_usage & 0xF0) == 0x50:
            self.availability = "used"
        elif (raw_usage & 0xF0) == 0xFF:
            self.availability = "rsvd"
            self.reserved = True
        else:
            logging.warning(f"{i}: unknown usage spec: 0x{raw_usage:02x}")
            self.availability = "????"

        if (raw_usage & 0x0F) == 0x00:
            self.link_mode = "unused"
        elif (raw_usage & 0x0F) == 0x01:
            self.link_mode = "first"
        elif (raw_usage & 0x0F) == 0x02:
            self.link_mode = "middle"
        elif (raw_usage & 0x0F) == 0x03:
            self.link_mode = "last"
        elif (raw_usage & 0x0F) == 0x0F:
            self.link_mode = "reserved"
            self.reserved = True
        else:
            logging.warning(f"{i}: unknown usage spec: 0x{raw_usage:02X}")
            self.link_mode = "????"

        if raw_reserved == b"\x00\x00\x00":
            if self.reserved:
                logging.warning(f"{i}: disagreement on reserved status")
        elif raw_reserved == b"\xFF\xFF\xFF":
            if not self.reserved:
                logging.warning(f"{i}: disagreement on reserved status")
                self.reserved = True

        if raw_chainlen % 0x2000:
            logging.warning(f"{i}: invalid chain length: 0x{raw_chainlen:06X}")
        self.chainlen = raw_chainlen // 0x2000

        if raw_link == 0xFFFF:
            self.link = None
        else:
            self.link = raw_link

        self.country = raw_country.rstrip(b"\x00").decode("ascii")
        self.product = raw_product.rstrip(b"\x00").decode("ascii")
        self.ident = raw_ident.rstrip(b"\x00").decode("ascii")

        if self.link_mode in ("middle", "last") or not self.product:
            self.name = ""
            self.name_raw = ""
        else:
            addr = slot_addr(i)
            addr_name_frame = slice(addr.start, addr.start + FRAME_SIZE)
            name_frame = mc[addr_name_frame]
            try:
                name = name_frame[0x4:0x44].decode("shift_jis")
            except UnicodeDecodeError:
                name = "(decode error)"
            self.name_raw = name

            # De-fullwidth...
            name_normalized = ""
            for i in name:
                cp = ord(i)
                if cp >= 0xFF00 and cp <= 0xFF53:
                    cp -= 0xFEE0
                elif cp == 0x3000:
                    cp = 0x20
                elif cp == 0x201D:
                    cp = 0x27
                name_normalized += chr(cp)

            self.name = name_normalized

    def printall(self):
        print(f"Directory entry: {self.n}")
        print(f"Name:           {self.name}")
        print(f"Name, original: {self.name_raw}")
        print(f"Reserved:       {self.reserved}")
        print(f"Availability:   {self.availability}")
        print(f"Link mode:      {self.link_mode}")
        print(f"Chain length:   {self.chainlen} blocks")
        print(f"Next link:      {self.link}")
        print(f"Country code:   {self.country}")
        print(f"Product code:   {self.product}")
        print(f"Identifier:     {self.ident}")

def open_mc(name, write):
    is_dev = name.startswith("/dev")
    if platform.system() == "Windows" and name.startswith("COM"):
        is_dev = True

    if is_dev:
        dd = DexDrive(name)
        buf = DexdriveRWBuffer(dd)
        return buf
    else:
        return MemcardFileRWBuffer(name, write=write)

def do_copy(src, dest, slots, only_used):
    """
    src: source path
    dest: destination path
    slots: list of slots to copy; None to copy all
    only_used: if true, only copy in-use slots
    """

    mc_from = open_mc(src, write=False)
    mc_to = open_mc(dest, write=True)

    if slots is None:
        slots = list(range(15))

    if only_used:
        if any(i not in range(15) for i in slots):
            raise Exception("Invalid slots: must be 0 - 14")

    for i in slots:
        if only_used:
            dirent = MemcardDir(mc_from, i)
            if dirent.availability == "free":
                logging.info(f"Skipping free slot {i}")
                continue

        status(f"Copying slot {i}...")

        dirframe = mc_from[dirframe_addr(i)]
        mc_to[dirframe_addr(i)] = dirframe
        block = mc_from[slot_addr(i)]
        mc_to[slot_addr(i)] = block
        mc_to.flush()

def do_wipe(card, quick):
    """
    card: path to card to wipe
    quick: whether to perform a quick wipe
    """

    mc_to = open_mc(card, write=True)
    bs = mc_to.block_size()
    num = 64 if quick else mc_to.len()
    for i in range(num):
        if i % 64 == 0:
            if i == 0:
                status("Wiping header slot...")
            else:
                status(f"Wiping slot {i // 64}")

        mc_to.write_block(i, bytes(bs))

def do_list(card):
    mc_from = open_mc(card, write=False)
    if mc_from[0:2] != b"MC":
        logging.warning("magic number 'MC' does not match, this may be a bad card")

    directory = [MemcardDir(mc_from, i) for i in range(15)]

    print("NUM USED LINK CTRY PROD       IDENT    NAME")
    for n, i in enumerate(directory):
        num = f"{n:3}"
        used = f"{i.availability:4}"
        link = f"{i.link:4}" if i.link is not None else "null"
        ctry = f"{i.country:4}"
        prod = f"{i.product:10}"
        ident = f"{i.ident:8}"
        name = i.name

        print(num, used, link, ctry, prod, ident, name)

def do_show(card, slots):
    if slots is None:
        slots = range(15)

    mc_from = open_mc(card, write=False)
    if mc_from[0:2] != b"MC":
        logging.warning("magic number 'MC' does not match, this may be a bad card")

    first = True
    for i in slots:
        if first:
            first = False
        else:
            print()
        dirent = MemcardDir(mc_from, int(i))
        dirent.printall()

def do_format(card):
    mc_to = open_mc(card, write=True)
    mc_to[0x00:0x80] = bytes(128)
    mc_to[0x00:0x02] = b"MC"
    xor_frame(mc_to, 0)

    for slot in range(15):
        addr = dirframe_addr(slot)
        mc_to[addr] = bytes(addr.stop - addr.start)
        mc_to[addr.start + 0x00] = 0xA0
        mc_to[addr.start + 0x08 : addr.start + 0x0A] = b"\xFF\xFF"
        xor_frame(mc_to, slot + 1)

    mc_to.flush()

if __name__ == "__main__":
    p = argparse.ArgumentParser(
        description="PSX DexDrive memory card tool.",
    )
    sp = p.add_subparsers(dest="command", required=True)

    p.add_argument(
        "--verbose", "-v",
        action="count",
        help="Increase verbosity",
        default=0,
    )

    p.add_argument(
        "--quiet", "-q",
        action="store_true",
        help="Silence status messages",
    )

    p.add_argument(
        "--quick", "-Q",
        action="store_true",
        help="Only access necessary blocks",
    )

    def slot_type(val):
        val = int(val)
        if val < 0 or val > 14:
            raise argparse.ArgumentTypeError("slots may only be from 0 to 14")
        return val

    def read_type(val):
        if os.access(val, os.R_OK):
            return val
        else:
            raise argparse.ArgumentTypeError(f"cannot read {val}")

    def write_type(val):
        if os.access(val, os.W_OK):
            return val
        else:
            raise argparse.ArgumentTypeError(f"cannot write {val}")

    p_list = sp.add_parser("list", help="List slots")
    p_list.add_argument("SRC", type=read_type, help="Card to read")

    p_show = sp.add_parser("show", help="Show detailed entries for slots")
    p_show.add_argument("SRC", type=read_type, help="Card to read")
    p_show.add_argument("SLOTS", nargs="+", type=slot_type, help="Slots to read")

    p_wipe = sp.add_parser("wipe", help="Wipe a card (or directory, with -Q)")
    p_wipe.add_argument("DEST", type=write_type, help="Card to write")

    p_format = sp.add_parser("format", help="Format a card")
    p_format.add_argument("DEST", type=write_type, help="Card to write")

    p_copy = sp.add_parser("copy", help="Copy slots between cards")
    p_copy.add_argument("SRC", type=read_type, help="Card to read")
    p_copy.add_argument("DEST", type=write_type, help="Card to write")
    p_copy.add_argument("SLOTS", nargs="+", type=slot_type, help="Slots to copy")

    args = p.parse_args()

    if args.quiet:
        QUIET = True

    if args.verbose == 1:
        logging.getLogger().setLevel(logging.INFO)
    elif args.verbose == 2:
        logging.getLogger().setLevel(logging.DEBUG)
    elif args.verbose > 2:
        print("moo")
        sys.exit()

    if args.command == "copy":
        do_copy(
            args.SRC,
            args.DEST,
            args.SLOTS or None,
            only_used=args.quick,
        )
    elif args.command == "wipe":
        do_wipe(
            args.DEST,
            quick=args.quick,
        )
    elif args.command == "list":
        do_list(args.SRC)
    elif args.command == "show":
        do_show(
            args.SRC,
            args.SLOTS or None,
        )
    elif args.command == "format":
        do_format(args.DEST)
